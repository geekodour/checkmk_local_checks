# check_mk_tests
Few Check MK tests for nagios

### Lists of tests to write:
1. smartctl alerts
  - This uses [S.M.A.R.T](https://wiki.archlinux.org/index.php/S.M.A.R.T.), it has two packages smartctl and smartd
  - We need tha health status in a breif format, also need info about pending sectors
  - Also smartmontools only works with physical devices, it does not know about partitions and file systems.

2. long mysql queries. something got stuck for 5 minutes, etc.
  - Use `mysqlslap` to simulate slow queries
  - `mysqlslap -u geekodour -p123 --concurrency=5 --iterations=200`

3. parsing apache logs, alert on too many 500 resources.
  - wip, apache log -> syslog-ng -> script -> nagios (might change)
  - spending some time understanding syslog-ng and friends as new to logging

4. payment checks. we do a fake payment every hour. we just send an email, no alert setup for it.
  - Can you provide more info on where can I look for this fake payment to monitor it?

5. list subscription rate.
  - Don't understand what exactly to do

6. XMPP and possibly the authentication service for it.
  - Don't understand what exactly to do

7. more consistent ssl cert checking of SSL certs. we have to enable each by hand.
  - I think this will need separate list of domain names for each host, should we use ansible here or what?

8. checks for ipv6 and ipv4 (both)
  - Don't understand what exactly to do

9. **we want as much as possible setup through ansible**
